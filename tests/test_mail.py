# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for Mail module """
import os
import sys
import tarfile
import shutil
import email
# from datetime import datetime
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, C0415, W0621, W0212, R0914
from bubblemail.config import Config as C
from bubblemail.account import Account as A
from bubblemail.mail import Mail as M
from bubblemail.localmail import LocalMail
from bubblemail.remotemail import IMAP, POP3

CACHE_DIR = '/tmp/bubblemail-test'
MAIL_STRING = """\
From: You <you@example.org>
To: Me <me@example.org>
Subject: Hello...
Date: Tue, 01 May 2018 16:28:08 +0300

...World!
"""
MAIL_TEMPLATE = email.message_from_string(MAIL_STRING)
MAIL_TEMPLATE2 = email.message_from_string(MAIL_STRING.replace('exam', 'toto'))
FAKE_HOME = '/tmp/bb_test_fake_home'

@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CACHE_DIR):
        shutil.rmtree(CACHE_DIR)
    return CACHE_DIR

def test_mail_from_raw():
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE)
    assert mail[M.UUID]
    assert mail[M.DATETIME] == '1525181288'
    assert mail[M.ACCOUNT] == 'MYACCUUID'
    assert mail[M.SUBJECT] == 'Hello...'
    assert mail[M.NAME] == 'You'
    assert mail[M.ADDRESS] == 'you@example.org'

def test_convert():
    malformed_text = '=?ISO-8859-1?Q?Hello?='
    assert M.convert(malformed_text) == 'Hello'
    malformed_text = '=?UTF-8?Q?Zg=C5=82oszenie__?='
    assert M.convert(malformed_text) == 'Zgłoszenie  '

def test_sort():
    def _replace(old, new):
        return email.message_from_string(MAIL_STRING.replace(old, new))
    oldest_mail = M.from_raw('a', '', MAIL_TEMPLATE)
    old_mail = M.from_raw('a', '', _replace('16:28', '17:28'))
    new_mail = M.from_raw('a', '', _replace('16:28', '19:15'))
    newest_mail = M.from_raw('a', '', _replace('01 May', '02 May'))
    sort_map = [oldest_mail, old_mail, new_mail, newest_mail]
    unsort_map = [new_mail, oldest_mail, newest_mail, old_mail]
    assert M.sort(unsort_map) == sort_map
    assert M.sort(unsort_map, reverse=True) == list(reversed(sort_map))

def test_collect_localmail(monkeypatch, mocker, fake_config_folder):
    for fake_localmail in 'fake_maildir', 'fake_mbox':
        mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
        if os.path.exists(FAKE_HOME):
            shutil.rmtree(FAKE_HOME)
        os.makedirs(FAKE_HOME)
        monkeypatch.setenv('HOME', FAKE_HOME)
        with tarfile.open(f'tests/{fake_localmail}.tar.xz') as tar_file:
            tar_file.extractall(FAKE_HOME)
        config = C()
        mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
        from bubblemail.mail import MailCache
        cache = MailCache()
        new_mails, unseen_mails = M.collect([], cache, config, first_check=True)
        assert len(new_mails) == 3
        assert len(unseen_mails) == 3
        new_mails, unseen_mails = M.collect([], cache, config)
        assert not new_mails
        assert len(unseen_mails) == 3
        config.set(C.CORE, C.LOCAL_MAIL, '0')
        new_mails, unseen_mails = M.collect([], cache, config, first_check=True)
        assert not new_mails and not unseen_mails

def test_collect_accounts(monkeypatch, mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    if os.path.exists(FAKE_HOME):
        shutil.rmtree(FAKE_HOME)
    fake_maildir = os.path.join(FAKE_HOME, 'fake_mail')
    os.makedirs(fake_maildir)
    monkeypatch.setenv('HOME', FAKE_HOME)
    for fake_localmail in 'fake_maildir', 'fake_mbox':
        with tarfile.open(f'tests/{fake_localmail}.tar.xz') as tar_file:
            tar_file.extractall(fake_maildir)
    config = C()

    class FakeImapMail(LocalMail):
        def __init__(self, unused_account):
            super().__init__(config)
            self.mdir_path = os.path.join(fake_maildir, 'Maildir')
            self.mbox_files = list()
        def close(self):
            pass

    class FakePopMail(LocalMail):
        def __init__(self, unused_account):
            super().__init__(config)
            self.mdir_path = None
            self.mbox_files = [os.path.join(fake_maildir, '.mbox')]

    M.BACKEND_MAP = {IMAP: FakeImapMail, POP3: FakePopMail}
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    cache = MailCache()
    account1 = A(config, name='a')
    account2 = A(config, name='b')
    account3 = A(config, name='c')
    account2[A.TYPE] = A.POP3
    new_mails, unseen_mails = M.collect([account1], cache, config,
                                        first_check=True)
    assert len(new_mails) == 3
    assert len(unseen_mails) == 3
    new_mails, unseen_mails = M.collect([account1], cache, config)
    assert not new_mails
    assert len(unseen_mails) == 3
    new_mails, unseen_mails = M.collect([account1, account2], cache, config,
                                        first_check=True)
    assert len(new_mails) == 6
    assert len(unseen_mails) == 6
    new_mails, unseen_mails = M.collect([account1, account2], cache, config)
    assert not new_mails
    assert len(unseen_mails) == 6
    new_mails, unseen_mails = M.collect([account1, account2, account3], cache,
                                        config, first_check=True)
    assert len(new_mails) == 9
    assert len(unseen_mails) == 9
    new_mails, unseen_mails = M.collect([account1, account2, account3], cache,
                                        config)
    assert not new_mails
    assert len(unseen_mails) == 9

def test_mailcache_load(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    fake_cache_file = os.path.join(CACHE_DIR, f'mails.cache')
    assert MailCache().load() is None
    with open(fake_cache_file, 'w') as corrupted_file:
        corrupted_file.write('always equal to \n the length\n\n of the string')
    mail_cache = MailCache()
    assert not bool(mail_cache)

def test_mailcache_add_delete(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    mail_cache.add(mail, dismissed=False)
    assert not mail_cache.is_dismissed(mail[M.UUID])
    assert mail_cache[mail[M.UUID]][1] == 'my acc'
    mail_cache.delete(mail[M.UUID])
    mail_cache.add(mail, dismissed=True)
    assert mail_cache.is_dismissed(mail[M.UUID])
    mail_cache.delete(mail[M.UUID])
    assert mail[M.UUID] not in mail_cache
    mail_cache.delete(mail[M.UUID])

def test_mailcache_sync(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    config = C()
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    account1 = A(config, name='a')
    mail1 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE)
    account2 = A(config, name='b')
    mail2 = M.from_raw(account2[A.UUID], '', MAIL_TEMPLATE2)
    mail_cache.add(mail1, dismissed=False)
    mail_cache.add(mail2, dismissed=False)
    mail_cache.sync([account1, account2], [])
    assert not mail_cache.contains(mail1[M.UUID])
    assert not mail_cache.contains(mail2[M.UUID])
    mail_cache = MailCache()
    account1 = A(config, name='a')
    mail1 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE)
    error_account = A(config, name='error_account')
    error_account[A.ERROR] = 'Error in account'
    error_account_mail = M.from_raw(error_account[A.UUID], '', MAIL_TEMPLATE2)
    mail_cache.add(mail1, dismissed=False)
    mail_cache.add(error_account_mail, dismissed=False)
    mail_cache.sync([account1, error_account], [])
    assert not mail_cache.contains(mail1[M.UUID])
    assert mail_cache.contains(error_account_mail[M.UUID])
    error_account[A.ERROR] = ''
    mail_cache.sync([error_account], [])
    assert not mail_cache.contains(error_account_mail[M.UUID])
    mail_cache = MailCache()
    mail_cache.add(error_account_mail, dismissed=True)
    error_account[A.ERROR] = 'Error in account'
    mail_cache.sync([error_account], [])
    assert error_account_mail[M.UUID] in mail_cache
    assert mail_cache.is_dismissed(error_account_mail[M.UUID])

def test_mailcache_save(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    mail_cache['710ff64b0d3708f91930cde16106c744'] = [False, 'my acc']
    mail_cache['710ff64b0d3708f91930cde16106c746'] = [True, 'my acc']
    mail_cache.save()
    mail_cache = MailCache()
    assert mail_cache['710ff64b0d3708f91930cde16106c744'] == [False, 'my acc']
    assert mail_cache['710ff64b0d3708f91930cde16106c746'] == [True, 'my acc']
    mail_cache['710ff64b0d3708f91930cde16106c744'][0] = True
    mail_cache['710ff64b0d3708f91930cde16106c746'][0] = False
    mail_cache.save()
    mail_cache = MailCache()
    assert mail_cache['710ff64b0d3708f91930cde16106c744'][0]
    assert not mail_cache['710ff64b0d3708f91930cde16106c746'][0]

def test_mailcache_dismiss(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    mail_cache.add(mail, dismissed=False)
    assert not mail_cache.is_dismissed(mail[M.UUID])
    mail_cache.dismiss(mail[M.UUID])
    assert mail_cache[mail[M.UUID]][0]
    assert mail_cache.is_dismissed(mail[M.UUID])
