# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for DbusService class """
import os
import sys
import shutil
import signal
from time import sleep
import threading
from gi.repository import GLib
import dbus
from dbus.mainloop.glib import DBusGMainLoop
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, W0621, W0212, W0231, W0233, R0903, R0914
from bubblemail.config import Config as C, PACKAGE_NAME
from bubblemail.dbusservice import DBusService
from bubblemail.mail import Mail as M
from test_mail import MAIL_TEMPLATE

CONF_DIR = '/tmp/bubblemail-test'
DBUS_BUS_NAME = f'{PACKAGE_NAME}Test.{PACKAGE_NAME.capitalize()}TestService'
DBUS_OBJ_PATH = f'/{DBUS_BUS_NAME.replace(".", "/")}'

class FakePlugin:
    def __init__(self):
        self.enabled = False
    def enable(self):
        self.enabled = True
    def disable(self):
        self.enabled = False

FAKE_PLUGIN1 = FakePlugin()

class FakeCache(dict):
    def dismiss(self, uuid):
        self[uuid] = 'dismissed'

class FakePluginAgregator(dict):
    def __init__(self):
        super().__init__()
        self.error = []
        self.reloaded = False
        self['fakeplugin1'] = FAKE_PLUGIN1
    def reload(self):
        self.reloaded = True

class FakeService:
    def __init__(self):
        self.config = None
        self.plugins = FakePluginAgregator()
        self.refreshed = False
        self.cache = FakeCache()
        self.got_stop_request = False
    def stop(self):
        self.got_stop_request = True
    def refresh(self, first_check=False, force=False):  # pylint: disable=W0613
        self.refreshed = True

class FakeDbusService(DBusService):
    def __init__(self, service):
        self.service = service
        self.mail_map = []
        bus_name = dbus.service.BusName(DBUS_BUS_NAME, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, DBUS_OBJ_PATH)

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def Refresh(self):
        super().Refresh()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def GetConfig(self):
        return super().GetConfig()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def SetConfig(self, serialized_config):
        return super().SetConfig(serialized_config)

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='aas')
    def GetPluginState(self):
        return super().GetPluginState()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='aa{sv}')
    def GetContent(self):
        return super().GetContent()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def DisablePlugin(self, plugin):
        super().DisablePlugin(plugin)

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def EnablePlugin(self, plugin):
        super().EnablePlugin(plugin)

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def Dismiss(self, uuid):
        super().Dismiss(uuid)

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def DismissAll(self):
        super().DismissAll()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def Shutdown(self):
        super().Shutdown()

class FakeDbusSignalListener:
    def __init__(self):
        self.signal_result = None
    def on_signal(self, signal_result):
        self.signal_result = signal_result

def service_thread(glib_loop):
    def sigterm_handler(mainloop):
        if mainloop is not None:
            mainloop.quit()
    DBusGMainLoop(set_as_default=True)
    GLib.unix_signal_add(GLib.PRIORITY_HIGH, signal.SIGTERM,
                         sigterm_handler, glib_loop)
    FAKE_SERVICE.config = C(dbus_service=FAKE_DBUS_SERVICE)
    glib_loop.run()

@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CONF_DIR):
        shutil.rmtree(CONF_DIR)
    return CONF_DIR

GLIB_LOOP = GLib.MainLoop()
DBUS_LOOP = dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
BUS = dbus.SessionBus(DBUS_LOOP)
FAKE_SERVICE = FakeService()
FAKE_DBUS_SERVICE = FakeDbusService(FAKE_SERVICE)

def test_dbus_running(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    threading.Thread(target=service_thread, args=(GLIB_LOOP,)).start()
    sleep(.2)
    assert BUS.name_has_owner(DBUS_BUS_NAME)

def test_dbus_getconfig(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    config = dbus_proxy.get_dbus_method('GetConfig', DBUS_BUS_NAME)()
    assert config == FAKE_SERVICE.config.serialize()

def test_dbus_setconfig(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    new_config = C()
    new_config.set(C.CORE, C.POLL_INTERVAL, '1')
    new_config = new_config.serialize()
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    dbus_proxy.get_dbus_method('SetConfig', DBUS_BUS_NAME)(new_config)
    config = dbus_proxy.get_dbus_method('GetConfig', DBUS_BUS_NAME)()
    assert config == new_config

def test_dbus_refresh(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    dbus_proxy.get_dbus_method('Refresh', DBUS_BUS_NAME)()
    assert FAKE_SERVICE.refreshed
    FAKE_SERVICE.refreshed = False

def test_dbus_getpluginstate(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    plugin_state = dbus_proxy.get_dbus_method('GetPluginState', DBUS_BUS_NAME)()
    assert not plugin_state #dbus.Array([], signature=dbus.Signature('s'))
    FAKE_SERVICE.plugins.error = ['pluginwitherror']
    plugin_state = dbus_proxy.get_dbus_method('GetPluginState', DBUS_BUS_NAME)()
    assert plugin_state[0] == ['pluginwitherror']
    FAKE_SERVICE.plugins.error = ['pluginwitherror1', 'pluginwitherror2']
    plugin_state = dbus_proxy.get_dbus_method('GetPluginState', DBUS_BUS_NAME)()
    assert plugin_state[0] == ['pluginwitherror1', 'pluginwitherror2']

def test_dbus_getcontent(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    mail_map = dbus_proxy.get_dbus_method('GetContent', DBUS_BUS_NAME)()
    assert mail_map == []
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    FAKE_DBUS_SERVICE.mail_map.append(mail)
    mail_map = dbus_proxy.get_dbus_method('GetContent', DBUS_BUS_NAME)()
    assert mail_map == [mail]
    FAKE_DBUS_SERVICE.mail_map.append(mail)
    mail_map = dbus_proxy.get_dbus_method('GetContent', DBUS_BUS_NAME)()
    assert mail_map == [mail, mail]

def test_dbus_enableplugin(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    FAKE_SERVICE.config = C()
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    FAKE_SERVICE.plugins.reloaded = False
    assert 'fakeplugin1' not in FAKE_SERVICE.config.get(
        C.CORE, C.ENABLED_PLUGINS)
    dbus_proxy.get_dbus_method('EnablePlugin', DBUS_BUS_NAME)('fakeplugin1')
    assert FAKE_SERVICE.plugins.reloaded
    assert 'fakeplugin1' in FAKE_SERVICE.config.get(C.CORE, C.ENABLED_PLUGINS)

def test_dbus_disableplugin(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    FAKE_SERVICE.config = C()
    FAKE_SERVICE.config.set(C.CORE, C.ENABLED_PLUGINS,
                            'libnotifyplugin, fakeplugin1')
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    FAKE_SERVICE.plugins.reloaded = False
    dbus_proxy.get_dbus_method('DisablePlugin', DBUS_BUS_NAME)('fakeplugin1')
    assert FAKE_SERVICE.plugins.reloaded
    assert 'fakeplugin1' not in FAKE_SERVICE.config.get(C.CORE,
                                                        C.ENABLED_PLUGINS)

def test_dbus_dismiss(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    FAKE_SERVICE.cache[mail[M.UUID]] = 'visible'
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    dbus_proxy.get_dbus_method('Dismiss', DBUS_BUS_NAME)(mail[M.UUID])
    assert FAKE_SERVICE.cache[mail[M.UUID]] == 'dismissed'
    dbus_proxy.get_dbus_method('Dismiss', DBUS_BUS_NAME)(mail[M.UUID])
    assert FAKE_SERVICE.cache[mail[M.UUID]] == 'dismissed'

def test_dbus_dismissall(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mail1 = M.from_raw('my acc', '', MAIL_TEMPLATE)
    mail2 = M.from_raw('my acc21', 'myfolder', MAIL_TEMPLATE)
    FAKE_SERVICE.cache[mail1[M.UUID]] = 'visible'
    FAKE_SERVICE.cache[mail2[M.UUID]] = 'visible'
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    dbus_proxy.get_dbus_method('DismissAll', DBUS_BUS_NAME)()
    assert FAKE_SERVICE.cache[mail1[M.UUID]] == 'visible'
    assert FAKE_SERVICE.cache[mail2[M.UUID]] == 'visible'
    dbus_proxy.get_dbus_method('DismissAll', DBUS_BUS_NAME)()
    assert FAKE_SERVICE.cache[mail1[M.UUID]] == 'visible'
    assert FAKE_SERVICE.cache[mail2[M.UUID]] == 'visible'

def test_dbus_shutdown(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    FAKE_SERVICE.got_stop_request = False
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    dbus_proxy.get_dbus_method('Shutdown', DBUS_BUS_NAME)()
    assert FAKE_SERVICE.got_stop_request

def test_dbus_contentupdate_signal(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    fake_dbus_listener = FakeDbusSignalListener()
    dbus_proxy.connect_to_signal('ContentUpdate', fake_dbus_listener.on_signal)
    mail1 = M.from_raw('my acc', '', MAIL_TEMPLATE)
    FAKE_DBUS_SERVICE.on_update([mail1])
    sleep(.2)
    assert fake_dbus_listener.signal_result == [mail1]

def test_dbus_configupdate_signal(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.set(C.CORE, C.POLL_INTERVAL, '1')
    dbus_proxy = BUS.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
    fake_dbus_listener = FakeDbusSignalListener()
    dbus_proxy.connect_to_signal('ConfigUpdate', fake_dbus_listener.on_signal)
    FAKE_DBUS_SERVICE.ConfigUpdate(config.serialize())
    sleep(.2)
    assert fake_dbus_listener.signal_result == config.serialize()

def test_dbus_stop():
    GLIB_LOOP.quit()
