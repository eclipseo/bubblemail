# -*- coding: utf-8 -*-
#
# test_account.py
#
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

"""Test cases for Account."""
# TODO: GOA tests
import os
import sys
import shutil
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413
from bubblemail.config import Config, FAKESPACE
from bubblemail.account import Account as A

# pylint: disable=W0621
@pytest.fixture
def fake_config_folder():
    if os.path.isdir('/tmp/bubblemail-test'):
        shutil.rmtree('/tmp/bubblemail-test')
    return '/tmp/bubblemail-test'

class FakeSecretStore:
    def save(self, account_name, uuid, password):
        pass

    def password(self, uuid):  # pylint: disable=R0201,W0613
        return 'secret'

    def clear(self, uuid):
        pass

@pytest.fixture
def fake_secret_store():
    return FakeSecretStore

def test_default_values(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config)
    assert len(account[A.UUID]) >= 8 and account.section and account[A.ENABLED]
    assert account[A.TYPE] == A.INTERNAL and account[A.BACKEND] == A.IMAP
    assert account[A.FOLDERS] == []

def test_null_folder_filter(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=['a', '', 'def'])
    assert account[A.FOLDERS] == ['a', 'def']

def test_account_folder_list_from_string(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, folders=f'a,r, f{FAKESPACE}with{FAKESPACE}s, c, def')
    assert account[A.FOLDERS] == ['a,r', 'f with s', 'c', 'def']

def test_account_config_section_name(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config, uuid='12345678')
    assert account.section == 'Account 12345678'

def test_account_uuid_is_unique(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account_map = [A(config, name='a', user='x', server='xx'),
                   A(config, name='b', backend=A.POP3, user='x', server='xx'),
                   A(config, name='c', user='x', server='xx')]
    assert len(set(a[A.UUID] for a in account_map)) == len(account_map)

def test_account_validation(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = Config()
    account = A(config)
    assert not account.is_valid
    account[A.NAME] = 'valid account'
    account[A.SERVER] = 'imap.valid.server.org'
    account[A.USER] = 'valid username'
    account[A.PASS] = 'validpassword'
    assert account.is_valid
    for key in A.NAME, A.SERVER, A.USER, A.PASS:
        valid_value = account[key]
        account[key] = ''
        assert not account.is_valid
        account[key] = valid_value
    assert account.is_valid
    account[A.BACKEND] = 'unsupportedBackend'
    assert not account.is_valid

def test_account_keep_configuration_across_save(mocker, fake_config_folder,
                                                fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_template = dict(
        name='my name', user='who', password='secret', server='example.org',
        port='124', folders=['a', 'b'], type=A.INTERNAL, backend=A.IMAP,
        enabled='0')
    account = A(config)
    for key, value in account_template.items():
        account[key] = value
    account.save()
    config.load()
    saved_account = A.get(config, account[A.UUID])
    for key, value in account_template.items():
        assert saved_account[key] == value

def test_account_list(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account_map = [A(config, name='a', user='x', server='xx'),
                   A(config, name='b', backend=A.POP3, user='x', server='xx'),
                   A(config, name='c', user='x', server='xx')]
    for account in account_map:
        account.save()
    assert list(A.list(config)) == account_map

def test_account_delete(mocker, fake_config_folder, fake_secret_store):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.account.SecretStore', fake_secret_store)
    config = Config()
    account = A(config)
    account_uuid = account[A.UUID]
    account.save()
    account = A.get(config, account_uuid)
    account.delete()
    assert not A.get(config, account_uuid)
