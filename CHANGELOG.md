# Changelog

## [v0.4] - 2020-01-15
 - Rename binaries and desktop files with simplicity in mind (a3d1d59e, 3694b860)
   - Service, formely `bubblemail-service` become `bubblemaild`
   - Settings, formely `bubblemail-settings` become `bubblemail`
 - Add new version checker using gitlab api, inform user for new releases via a dialog (4adfd984)
 - Avoid avatar generation error if libfolks not installed, checking for it's installation at first (f842675a 4adfd984)
 - Fix bug providing avatar file that doesn't exist anymore, revert to default ones in this case (f842675a)
 - Fix bug dismissed mails are forgotten in case of account connexion error between checks (051d364a)
 - Fix bug: undo after account deletion not working (62bd432f)
 - Fix bug: Locale po files outdated (ec0e5b08)
 - Various clean ups

## [v0.3] - 2019-12-16
 - Add default colorized avatars based on sender name or address (c9c6c09b bcec0a2c)
 - Improve remote mail error reporting (c61ad860)
 - Add insecure connection warning (023c925c)
 - Add reverse order option for libnotify plugin (c9c6c09b)
 - Add config version property & check for config file incompatibity (4ca83d37)
 - Add dbus method & signal providing config (406c8496)
   - Used by gs extension to check version compatibility and account error reporting
 - Use dbus string array signature everywere (a9fe72d1)
 - Use account uuid instead of account name for mail to account relationship (a9fe72d1)
 - Use xdg cache folder instead of config for logs and caches (10c0b4c0)
 - Use pickle for mail cache (10c0b4c0)
 - Fix bug: version not always checked (fdfa357e)
 - Various clean ups