# Copyright 2011 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
# Copyright 2011 Leighton Earl <leighton.earl@gmx.com>
# Copyright 2011 Ralf Hersel <ralf.hersel@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import os
import logging
import hashlib
import email
from email.header import make_header, decode_header
from uuid import uuid4 as generate_uuid
from .config import CACHE_DIR
from .utils import get_avatars, get_data_file, pickle_read, pickle_write
from .localmail import LocalMail
from .remotemail import IMAP, POP3, RemoteImap, RemotePop3


class Mail(dict):
    """ Mail dictionary storage helper
            - All accounts collect method
            - From email object raw creation
            - Headers storage as dictionnary
    """
    DATETIME = 'datetime'
    SUBJECT = 'subject'
    NAME = 'name'
    ADDRESS = 'address'
    AVATAR = 'avatar'
    UUID = 'uuid'
    ACCOUNT = 'account'
    KEYS = (UUID, ACCOUNT, DATETIME, SUBJECT, NAME, ADDRESS)
    BACKEND_MAP = {IMAP: RemoteImap, POP3:RemotePop3}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self:
            for mailkey in self.KEYS:
                self[mailkey] = None

    def __eq__(self, other):
        """ compare itself with an uuid string """
        if isinstance(other, str):
            return other == self[self.UUID]
        return super().__eq__(other)

    @property
    def sender(self):
        return self[self.NAME] or self[self.ADDRESS]

    @staticmethod
    def from_raw(account, folder, raw_email):
        """ Create new instance from raw  """
        mail = Mail()
        mail[Mail.ACCOUNT] = account
        mail[Mail.SUBJECT] = mail.convert(raw_email['Subject'])
        mail_from = email.utils.parseaddr(raw_email['From'])
        if len(mail_from) != 2:
            logging.error(f'Malformed "From" header: {mail_from}')
            return None
        mail_from_conversion = map(mail.convert, mail_from)
        mail[Mail.NAME] = next(mail_from_conversion)
        mail[Mail.ADDRESS] = next(mail_from_conversion)
        parsed_date = email.utils.parsedate_tz(raw_email['Date'])
        # Datetime in unix format
        mail[Mail.DATETIME] = str(email.utils.mktime_tz(parsed_date))
        try:
            mail[Mail.UUID] = hashlib.md5(
                (account + folder + mail[Mail.NAME] + mail[Mail.ADDRESS]
                 + mail[Mail.SUBJECT] + mail[Mail.DATETIME])
                .encode('utf-8')).hexdigest()
        except IndexError:
            logging.error(f'Generating uuid for {mail}', exc_info=True)
            mail[Mail.UUID] = str(generate_uuid())[:8]
        return mail

    @classmethod
    def convert(cls, input_text):
        """ return utf-8 decoded string from charset specific input_text """
        decoded = decode_header(input_text)
        if not decoded[0][1] or 'unknown' in decoded[0][1]:
            decoded = [(decoded[0][0], 'latin-1')]
        return str(make_header(decoded))

    @staticmethod
    def sort(mail_map, reverse=False):
        return sorted(mail_map, key=lambda m: int(m[m.DATETIME]),
                      reverse=reverse)

    @staticmethod
    def collect(accounts, cache, config, first_check=False):  # pylint: disable=R0914
        """ Collect new messages from local and remote accounts """
        logging.info('Collecting mail...')
        mail_map = []
        # Avatar Update
        avatar_map = get_avatars()

        def append(account, folder, message):
            mail = Mail.from_raw(account, folder, message)
            if not mail:
                return
            if list(filter(lambda m: m[m.UUID] == mail[mail.UUID], mail_map)):
                logging.warning(f'Duplicate email detected: {mail}')
                return
            if avatar_map and mail[mail.ADDRESS].lower() in avatar_map:
                mail[mail.AVATAR] = avatar_map[mail[mail.ADDRESS].lower()]
            else:
                default_avatar = get_data_file(
                    f'avatars/{mail.sender[0].upper()}.png')
                mail[mail.AVATAR] = default_avatar or ''
            mail_map.append(mail)

        # Local mail
        localmail_count = 0
        localmail_set = int(config.get_core(config.LOCAL_MAIL))
        localmail = LocalMail(config)
        if localmail.available and localmail_set:
            logging.error('looking for localmail')
            for folder, message in localmail.list_messages():
                append(localmail.LOCAL, folder, message)
            localmail_count = len(mail_map)
            logging.info('Local account have %s unread mails', localmail_count)
        elif localmail_set:
            config.set_core(config.LOCAL_MAIL, False)
            config.save()
            logging.info('No local mail paths available')
        # Remote mail
        for account in accounts:
            if not int(account[account.ENABLED]):
                logging.debug('Account %s is disabled', account[account.NAME])
                continue
            mailbox = Mail.BACKEND_MAP[account[account.BACKEND]](account)
            for folder, message in mailbox.list_messages():
                append(account[account.UUID], folder, message)
            mailbox.close()
        sorted_map = Mail.sort(mail_map)
        unseen_map = list(filter(
            lambda m: not(cache.is_dismissed(m[m.UUID])), sorted_map))
        new_map = unseen_map if first_check else list(filter(
            lambda m: not cache.contains(m[m.UUID]), sorted_map))
        cache.sync(accounts, mail_map)
        if accounts:
            logging.info(f'{len(accounts)} remote account(s) have '
                         + f'{len(unseen_map)} unseen mails and '
                         + f'{len(new_map)} new mails')
        return new_map, unseen_map


class MailCache(dict):
    def __init__(self):
        dict.__init__(self)
        self.filepath = os.path.join(CACHE_DIR, 'mails.cache')
        self.account_map = dict()
        self.load()

    def load(self):
        self.clear()
        cache_content = pickle_read(self.filepath)
        if not cache_content:
            return
        for uuid, cache_data in cache_content.items():
            self[uuid] = cache_data

    def save(self):
        pickle_write(self, self.filepath)

    def sync(self, accounts, mail_map):
        error_accounts = filter(lambda a: a[a.ERROR], accounts)
        error_accounts = list(map(lambda a: a[a.UUID], error_accounts))
        deleted_uuids = filter(lambda m: m not in mail_map, self)
        deleted_uuids = list(filter(
            lambda u: self[u][1] not in error_accounts, deleted_uuids))
        deleted_uuids = list(map(self.delete, deleted_uuids))
        new_mails = filter(lambda m: not self.contains(m[m.UUID]), mail_map)
        new_mails = list(map(self.add, new_mails))
        if new_mails or deleted_uuids:
            logging.debug('Saving cache')
            self.save()

    def contains(self, uuid):
        """ check if mail id is in the cache list """
        return uuid in self

    def add(self, item, dismissed=False):
        self[item[Mail.UUID]] = [dismissed, item[Mail.ACCOUNT]]

    def delete(self, item):
        if item not in self:
            logging.warning(f'Mail with uuid {item} not found')
            return
        del self[item]

    def dismiss(self, uuid, commit=True):
        if self.is_dismissed(uuid):
            logging.warning(f'Mail with id {uuid} already dismissed')
            return
        self[uuid][0] = True
        if commit:
            self.save()

    def dismiss_all(self):
        """ Set dismissed flag on every cache items"""
        for uuid in self:
            self.dismiss(uuid, commit=False)
        self.save()

    def is_dismissed(self, uuid):
        if not self.contains(uuid):
            return False
        return self[uuid][0]
