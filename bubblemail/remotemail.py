# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

"""Backends to implement mail box specific functionality, like IMAP and POP3."""

from abc import ABCMeta, abstractmethod
import re
import email
import logging
import imaplib
import poplib
from ssl import SSLError
import socket

from .account import Account as A
from .config import NEWLINE
from .i18n import _

IMAP = 'imap'
POP3 = 'pop3'
BACKEND_MAP = {IMAP: imaplib.IMAP4, POP3: poplib.POP3}
SSL_BACKEND_MAP = {IMAP: imaplib.IMAP4_SSL, POP3: poplib.POP3_SSL}


class RemoteMail:
    """Interface for mailbox backends.
    Mailbox backend implements access to the specific type of mailbox.
    """

    __metaclass__ = ABCMeta

    FAILED = 1
    REFUSED = 2
    BAD_LOGIN = 3
    UNSECURE = 4
    CONNECT_ERROR = 5
    TIMEOUT = 6
    OFFLINE = 101
    NOCRED_ERR = _('No credentials found !')
    AUTH_ERR = _('Authentification failed')
    INSECURE_ERR = _('Insecure connection')
    TIMEOUT_ERR = _('Connection timeout')
    REFUSED_ERR = _('Connection refused')
    UNKNOWN_ERR = _('Unknown error')

    def __init__(self, account):
        self.account = account
        self.backend = BACKEND_MAP[self.account[A.BACKEND]]
        self.ssl_backend = SSL_BACKEND_MAP[self.account[A.BACKEND]]
        self.connection = None
        self.status = None
        socket.setdefaulttimeout(10)

    def open(self):  # pylint: disable=R0912, R0915
        """ Opens the mailbox. """

        if self.account.is_locked:
            logging.warning(f'Account {self.account[A.NAME]}: avoiding connect '
                            + f'attempt without credentials !!!')
            if self.account[A.ERROR] != self.NOCRED_ERR:
                self.account[A.ERROR] = self.NOCRED_ERR
                self.account.save()
            return None
        account_error = ''
        account_name = self.account[A.NAME]
        server = self.account[A.SERVER]
        port = self.account[A.PORT]
        if self.connection:
            logging.warning(f'{account_name}({self.account[A.TYPE]}): '
                            + f'connection on {server} is already open')
            return self.connection
        self.status = None

        def _open(backend, server, port):
            if port:
                self.connection = backend(server, port)
            else:
                self.connection = backend(server)
        try:
            _open(self.ssl_backend, server, port)
        except SSLError:
            try:
                _open(self.backend, server, port)
            except ConnectionRefusedError:
                self.status = self.BAD_LOGIN
            except socket.gaierror:
                self.status = self.REFUSED
            else:
                self.status = self.UNSECURE
        except ConnectionRefusedError:
            self.status = self.BAD_LOGIN
        except socket.gaierror as gaierror:
            self.status = gaierror.errno
            account_error = gaierror.strerror
        except OSError as oserror:
            self.status = oserror.errno
            account_error = oserror.strerror
        log_err = f'{self.account[A.BACKEND]} server {server} from '
        log_err += f'account {account_name}: '
        if self.status == self.REFUSED or self.status == self.BAD_LOGIN:
            logging.error(log_err + self.REFUSED_ERR)
            account_error = self.REFUSED_ERR
        elif self.status == self.BAD_LOGIN:
            logging.error(log_err + self.AUTH_ERR)
            account_error = self.AUTH_ERR
        elif self.status == self.UNSECURE:
            logging.warning(log_err + self.INSECURE_ERR)
            account_error = self.INSECURE_ERR
        if account_error != self.account[A.ERROR]:
            self.account[A.ERROR] = account_error
            self.account.save()
        return self.connection

    @abstractmethod
    def close(self):
        """Closes the mailbox."""
        if not self.connection:
            return None
        return True

    @abstractmethod
    def is_open(self):
        """Returns true if mailbox is open."""
        raise NotImplementedError

    @abstractmethod
    def list_messages(self):
        """Lists unseen messages from the mailbox for this account.
           Yields tuples (folder, message) for every message.
           Be aware that return value need to be iterable
        """
        self.connection = self.connection if self.connection else self.open()
        if not self.is_open():
            return None
        return True

    @abstractmethod
    def request_folders(self):
        """Returns list of folder names available in the mailbox.
        Raises an exceptions if mailbox does not support folders.
        """
        raise NotImplementedError


class RemoteImap(RemoteMail):
    """Implementation of IMAP mail boxes."""

    def open(self):
        if not super().open():
            return None
        account_error = ''
        err_str = f'{self.account[A.NAME]}({self.account[A.SERVER]}):'
        try:
            if A.OAUTH2 in self.account and self.account[A.OAUTH2]:
                self.connection.authenticate(
                    'XOAUTH2', lambda x: self.account[A.OAUTH2])
            else:
                self.connection.login(self.account[A.USER],
                                      self.account[A.PASS])
        except socket.timeout:
            logging.error(f'{err_str} timeout')
            self.status = self.TIMEOUT
            account_error = self.TIMEOUT_ERR
        except imaplib.IMAP4.error:
            logging.error(f'{err_str} bad credentials', exc_info=True)
            self.status = self.BAD_LOGIN
            account_error = self.AUTH_ERR
        except Exception:
            logging.error(f'{err_str} unhandled exception', exc_info=True)
            self.status = self.CONNECT_ERROR
            account_error = self.UNKNOWN_ERR
        else:
            return self.connection
        if account_error != self.account[A.ERROR]:
            self.account[A.ERROR] = account_error
            self.account.save()
        return None

    def close(self):
        if not super().close():
            return
        self.connection.select()
        self.connection.close()
        self.connection.logout()
        self.connection = None

    def is_open(self):
        return self.connection and self.connection.state != 'LOGOUT'

    def list_messages(self):
        if not super().list_messages():
            return None
        folder_map = ['INBOX']
        if A.FOLDERS in self.account and self.account[A.FOLDERS]:
            folder_map = self.account[A.FOLDERS]
        for folder in folder_map:
            self.connection.select(f'"{folder}"', readonly=True)
            try:
                status, data = self.connection.search(None, 'UNSEEN')
            except imaplib.IMAP4.error:
                logging.warning(f'Folder {folder} not found, removing it')
                self.account[A.FOLDERS].remove(folder)
                self.account.save()
                continue
            if status != 'OK' or data == b'':
                logging.debug(f'Folder:{folder}, status:{status}, data:{data}')
                continue # Bugfix LP-735071
            for num in data[0].split():
                # header only (without setting READ flag)
                _, msg_data = self.connection.fetch(num, '(BODY.PEEK[HEADER])')
                for part in msg_data:
                    if (not isinstance(part, (tuple, list)) or len(part) < 2
                            or not isinstance(part[1], (bytes, bytearray))):
                        continue
                    msg = email.message_from_bytes(part[1])
                    yield (folder, msg)
        return []

    def request_folders(self):
        self.connection = self.connection if self.connection else self.open()
        if not self.is_open():
            return []
        try:
            _unused, data_map = self.connection.list()
        finally:
            self.close()
        folder_list = list()
        for data in data_map:
            match = re.match(r'.+\s+("."|"?NIL"?)\s+"?([^"]+)"?$',
                             data.decode('utf-8'))
            if not match:
                logging.warning(f'{self.account[A.SERVER]}: bad folder format')
                continue
            folder = match.group(2)
            folder_list.append(folder)
        return folder_list


class RemotePop3(RemoteMail):
    """Implementation of POP3 mail boxes."""

    def open(self):
        if not super().open():
            return None
        err_str = f'{self.account[A.NAME]}({self.account[A.SERVER]}):'
        try:
            self.connection.getwelcome()
            self.connection.user(self.account[A.USER])
            self.connection.pass_(self.account[A.PASS])
        except poplib.error_proto:
            self.status = self.BAD_LOGIN
            logging.error(f'{err_str} bad credentials')
            self.account[A.ERROR] = self.AUTH_ERR
        except Exception:
            logging.error(f'{err_str} unhandled error', exc_info=True)
            self.account[A.ERROR] = self.UNKNOWN_ERR
        else:
            return self.connection
        if self.account[A.ERROR]:
            self.account.save()
        return None

    def close(self):
        if not super().close():
            return
        self.connection.quit()
        self.connection = None

    def is_open(self):
        return self.connection and 'sock' in self.connection.__dict__

    def list_messages(self):
        if not super().list_messages():
            return []
        folder = ''
        # number of mails on the server
        mail_total = len(self.connection.list()[1])
        for i in range(1, mail_total + 1): # for each mail
            try:
                # header plus first 0 lines from body
                message = self.connection.top(i, 0)[1]
                logging.debug(f'Pop3 message: {message}')
            except Exception:  # pylint: disable=W0703
                logging.error('Error getting POP message', exc_info=True)
                continue
            # convert list to string
            message = NEWLINE.join(filter(len, message))
            try:
                # put message into email object and make a dictionary
                msg = dict(email.message_from_bytes(message))
            except Exception:  # pylint: disable=W0703
                logging.error(f'Formating error for POP message {msg}')
                continue
            yield (folder, msg)
        return []

    def request_folders(self):
        raise NotImplementedError('POP3 does not support folders')
