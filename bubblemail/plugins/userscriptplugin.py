#!/usr/bin/env python3
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import os
import subprocess

from bubblemail.plugin import Plugin
from bubblemail.i18n import _


class UserscriptPlugin(Plugin):
    MANIFEST = (
        _('User Script'), _('Runs an user defined script on mail arrival.'),
        '1.2.1', 'Patrick Ulbrich <zulu99@gmx.net>, Razer <razerraz@free.fr')
    DEFAULT_CONFIG = {'script_file': ''}

    @property
    def filechooser(self):
        return self.builder.get_object('filechooser')

    def load_settings(self):
        if self.config['script_file']:
            self.filechooser.set_filename(self.config['script_file'])

    def save_settings(self):
        self.config['script_file'] = self.filechooser.get_filename() or ''

    def on_update(self, mails):
        new_mails, unseen_mails = mails
        script_file = self.config['script_file'].strip()
        if script_file and os.path.exists(script_file):
            script_args = [script_file, str(len(new_mails))]
            for mail in new_mails:
                script_args.append(mail[mail.ACCOUNT])
                script_args.append(f'{mail[mail.NAME]} <{mail[mail.ADDRESS]}>')
                script_args.append(mail[mail.SUBJECT])
            subprocess.Popen(script_args)
        return new_mails, unseen_mails
