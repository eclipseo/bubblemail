# Copyright 2011 - 2017 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import threading
# import time
import logging  # pylint: disable=W0611
from copy import copy
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
# pylint: disable=C0413
from gi.repository import Gdk, Gtk, GLib

from ..config import PACKAGE_NAME
from ..utils import get_data_file
from ..i18n import _
from ..account import Account as A
from ..localmail import LocalMail
from ..remotemail import IMAP, POP3, RemoteImap, RemotePop3
from ..dbusservice import CFG_SET_METHOD
from ..ui import check_ready

BACKEND_MAP = {IMAP: RemoteImap, POP3:RemotePop3}

class AccountDialog:  # pylint: disable=R0902,R0904
    def __init__(self, parent, account=None, config=None):
        self.parent = parent
        self.original_account = account
        self.config = config
        self.account = None
        self.ready = False
        self.remote = None
        self.folder_list = None
        self.user_edit = list()
        self.valid_entries = list()
        self.close_request = False
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('account.ui'))
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path(get_data_file('account.css'))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        builder.connect_signals({
            'on_chk_port_toggled' : self.on_chk_port_toggled,
            'on_port_value_changed': self.on_port_value_changed,
            'on_ent_server_changed': self.on_ent_server_changed,
            'on_ent_user_changed': self.on_ent_user_changed,
            'on_ent_pass_changed': self.on_ent_pass_changed,
            'on_ent_focus_change': self.on_ent_focus_change,
            'on_ent_name_changed': self.on_ent_name_changed,
            'on_backend_changed': self.on_backend_changed,
            'on_folders_activate': self.on_folders_activate,
            'close_test_dialog': self.close_test_dialog})
        self.widget = builder.get_object
        self.window = Gtk.Dialog(
            _('New account'), self.parent.window, None,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OK, Gtk.ResponseType.OK), use_header_bar=True)

        content = self.window.get_content_area()
        content.add(builder.get_object('dialog_content'))
        self.widget('test_dialog').set_transient_for(self.window)
        # Prepare folders treeview
        renderer_folders_enabled = Gtk.CellRendererToggle()
        renderer_folders_enabled.connect('toggled', self.on_folder_toggled)
        column_folders_enabled = Gtk.TreeViewColumn(
            _('Enabled'), renderer_folders_enabled)
        column_folders_enabled.add_attribute(
            renderer_folders_enabled, 'active', 0)
        column_folders_enabled.set_alignment(0.5)
        self.widget('treeview_folders').append_column(column_folders_enabled)
        renderer_folders_name = Gtk.CellRendererText()
        column_folders_name = Gtk.TreeViewColumn(
            _('Folder name'), renderer_folders_name, text=1)
        self.widget('treeview_folders').append_column(column_folders_name)
        self.window.set_response_sensitive(Gtk.ResponseType.OK, False)
        self.init_account(account)
        self.ready = True
        self.parent.config_lock = True
        if self.window.run() == Gtk.ResponseType.OK:
            self.on_ok_clicked()
        else:
            self.window.destroy()

    def init_account(self, account):
        if account:
            self.account = copy(account)
            if self.account[A.NAME]:
                self.window.set_title(
                    _('Edit {0} account').format(self.account[A.NAME]))
            if self.account[A.TYPE] == A.GOA:
                # Goa account : only show folders widget
                self.account.get_credentials()
                self.widget('box_settings').set_visible(False)
                self.widget('expander_folders').set_expanded(True)
                threading.Thread(target=self.get_remote).start()
                self.show_test_dialog()
            else:
                self.sync_account_settings()
        else:
            # Local Maildir
            self.window.set_title(_('Local maildir folders'))
            self.widget('box_settings').set_visible(False)
            self.widget('expander_folders').set_expanded(True)
            self.append_folders(LocalMail(self.config).request_folders(),
                                self.config.get_core(self.config.LOCAL_FOLDERS))

    def sync_account_settings(self):
        self.widget('ent_name').set_text(self.account[A.NAME] or '')
        self.widget('ent_server').set_text(self.account[A.SERVER] or '')
        self.widget('ent_user').set_text(self.account[A.USER] or '')
        self.widget('ent_pass').set_text(self.account[A.PASS] or '')
        self.widget('cmb_backend').set_active(
            int(self.account[A.BACKEND] == A.POP3))
        if self.account[A.PORT]:
            self.widget('chk_port').set_active(True)
            self.widget('spn_port').set_value(int(self.account[A.PORT]))
        else:
            self.widget('chk_port').set_active(False)
        for entry_id in ('ent_name', 'ent_server', 'ent_user', 'ent_pass'):
            if self.widget(entry_id).get_text():
                self.valid_entries.append(entry_id)
        self.widget('expander_folders').set_sensitive(
            self.account[A.BACKEND] == A.IMAP and len(self.valid_entries) == 4)

    @check_ready
    def on_folder_toggled(self, cell, path):
        str_folders = self.widget('str_folders')
        folder = str_folders.get_value(str_folders.get_iter(path), 1)
        isactive = not cell.get_active()
        str_folders.set_value(str_folders.get_iter(path), 0, isactive)
        if self.account:
            selected_folders = self.account[A.FOLDERS]
        else:
            selected_folders = self.config.get_core(self.config.LOCAL_FOLDERS)
        if isactive and folder not in selected_folders:
            selected_folders.append(folder)
        if not isactive and folder in selected_folders:
            selected_folders.remove(folder)
        if not self.account:
            self.config.set_core(self.config.LOCAL_FOLDERS, selected_folders)
        self.window.set_response_sensitive(Gtk.ResponseType.OK, True)


    def on_ok_clicked(self):
        self.close_request = True
        self.parent.config_lock = False
        if self.account:
            self.widget('test_spinner').start()
            self.widget('test_dialog').show()
            threading.Thread(target=self.get_remote).start()
        else:
            self.window.destroy()
            self.parent.dbus_send(CFG_SET_METHOD,
                                  arg=self.config.serialize())
            self.parent.dbus_send('Refresh')

    @check_ready
    def on_ent_focus_change(self, _unused_entry, _unused_direction):
        for entry_name in ('ent_name', 'ent_server', 'ent_user', 'ent_pass'):
            entry = self.widget(entry_name)
            context = entry.get_style_context()
            if not hasattr(entry, 'isvalid'):
                entry.isvalid = (True, '')
            if entry_name not in self.user_edit:
                continue
            if entry.has_focus() or entry.isvalid[0]:
                context.remove_class('invalid_entry')
            if not entry.has_focus() and not entry.isvalid[0]:
                context.add_class('invalid_entry')


    @check_ready
    def on_ent_server_changed(self, entry):
        if self.validate_entry(entry):
            self.account[A.SERVER] = entry.get_text()

    @check_ready
    def on_ent_user_changed(self, entry):
        if self.validate_entry(entry):
            self.account[A.USER] = entry.get_text()

    @check_ready
    def on_ent_pass_changed(self, entry):
        if self.validate_entry(entry, printable=False):
            self.account[A.PASS] = entry.get_text()

    @check_ready
    def on_ent_name_changed(self, entry):
        if self.validate_entry(entry, nospace=False):
            self.account[A.NAME] = entry.get_text()

    @check_ready
    def on_chk_port_toggled(self, checkbtn):
        toggled = checkbtn.get_active()
        self.widget('spn_port').set_sensitive(toggled)
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           len(self.valid_entries) == 4)
        if toggled:
            self.account[A.PORT] = str(int(self.widget('spn_port').get_value()))
        else:
            self.account[A.PORT] = None

    @check_ready
    def on_port_value_changed(self, spinbtn):
        self.account[A.PORT] = str(int(spinbtn.get_value()))
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           len(self.valid_entries) == 4)

    @check_ready
    def on_backend_changed(self, combo):
        backend = combo.get_active_text().lower()
        self.account[A.BACKEND] = backend
        self.widget('expander_folders').set_sensitive(
            backend == A.IMAP and len(self.valid_entries) == 4)
        if backend != A.IMAP or len(self.valid_entries) != 4:
            self.widget('expander_folders').set_expanded(False)

    @check_ready
    def on_folders_activate(self, expander):
        if expander.get_expanded():
            return
        if not self.remote or not self.folder_list:
            self.show_test_dialog()
            threading.Thread(target=self.get_remote).start()
        else:
            self.set_folder_widget_height(self.folder_list)

    def show_test_dialog(self):
        self.widget('test_dialog').set_title(_('Getting folder list...'))
        self.widget('lbl_error').hide()
        self.widget('test_spinner').show()
        self.widget('test_spinner').start()
        self.widget('test_dialog').set_keep_above(True)
        self.widget('test_dialog').show()

    def close_test_dialog(self, dialog=None, _unused_data=None):
        dialog = dialog or self.widget('test_dialog')
        self.widget('test_spinner').stop()
        dialog.hide()

    @check_ready
    def validate_entry(self, entry, lmin=3, printable=True, nospace=True):
        entry_name = Gtk.Buildable.get_name(entry)
        if entry_name not in self.user_edit:
            self.user_edit.append(entry_name)
        entry.isvalid = (True, None)
        entry_text = entry.get_text()
        if not entry_text or entry_text.isspace():
            entry.isvalid = (False, _('Entry is empty'))
        if len(entry_text) < lmin:
            entry.isvalid = (False, _('Entry is too short'))
        if printable and not entry_text.isprintable():
            entry.isvalid = (False, _('Entry contain incorrect caracters'))
        if nospace and ' ' in entry_text:
            entry.isvalid = (False, _('Entry contain spaces'))
        if entry.isvalid[0]:
            entry.set_tooltip_text('')
            if entry_name not in self.valid_entries:
                self.valid_entries.append(entry_name)
        else:
            entry.set_tooltip_text(entry.isvalid[1])
            if entry_name in self.valid_entries:
                self.valid_entries.remove(entry_name)
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           len(self.valid_entries) == 4)
        self.widget('expander_folders').set_sensitive(
            self.account[A.BACKEND] == A.IMAP and len(self.valid_entries) == 4)
        return entry.isvalid[0]

    def set_error_dialog(self):
        self.widget('test_dialog').set_title(_('Error'))
        self.widget('lbl_error').set_text(_('Connection failed'))
        if self.remote.status == self.remote.REFUSED:
            self.widget('lbl_error').set_text(_('Connection refused'))
        if self.remote.status == self.remote.BAD_LOGIN:
            self.widget('lbl_error').set_text(
                _('Incorrect username or password'))
        if self.remote.status >= self.remote.UNSECURE:
            self.widget('test_dialog').set_title(_('Warning'))
            self.widget('lbl_error').set_text(_('Unsecure connection'))
        self.widget('test_spinner').hide()
        self.widget('lbl_error').show()

    def get_remote(self):
        # time.sleep(10)
        self.remote = BACKEND_MAP[self.account[A.BACKEND]](self.account)
        self.remote.open()
        if self.remote.status:
            GLib.idle_add(self.set_error_dialog)
            if self.remote.status < self.remote.UNSECURE:
                self.close_request = False
                return
        else:
            GLib.idle_add(self.close_test_dialog)
        if self.close_request:
            GLib.idle_add(self.window.destroy)
            self.original_account = self.account
            self.original_account.save(commit=False)
            self.parent.dbus_send(CFG_SET_METHOD,
                                  arg=self.parent.config.serialize())
            self.parent.dbus_send('Refresh')
            GLib.idle_add(self.parent.ui_update)
            return
        if self.account[A.BACKEND] == A.IMAP:
            self.folder_list = self.remote.request_folders()
            GLib.idle_add(self.append_folders, self.folder_list,
                          self.account[A.FOLDERS])

    def append_folders(self, folder_list, enabled_folders):
        for folder in folder_list:
            if folder.lower() == 'trash' or folder.lower() == 'sent':
                continue
            folder = folder or 'Inbox'
            self.widget('str_folders').append(
                [folder in enabled_folders, folder])
        self.set_folder_widget_height(folder_list)

    def set_folder_widget_height(self, folder_list):
        if not folder_list:
            self.widget('scw_folders').set_min_content_height(50)
            return
        height = len(folder_list)*25
        height = 50 if height < 50 else height
        height = 250 if height > 250 else height
        self.widget('scw_folders').set_min_content_height(height)
