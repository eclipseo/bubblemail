# Copyleft 2019 razer <razerraz@free.fr>
# Copyright 2011 - 2016 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import os
import time
import builtins
import logging
from logging.handlers import RotatingFileHandler
import pickle
from datetime import datetime, timedelta
from subprocess import Popen, PIPE, TimeoutExpired
from distutils.sysconfig import get_python_lib
import dbus
import xdg.BaseDirectory as base
import requests
from requests.exceptions import ConnectionError as ConnErr, ConnectTimeout

from .config import (PACKAGE_NAME, DEVMODE, CONFIG_DIR, CACHE_DIR,
                     DBUS_BUS_NAME, DBUS_OBJ_PATH)

BIN_DIR = 'build/bin' if DEVMODE else '/usr/bin'
LOCALE_DIR = 'build/locale' if DEVMODE else '/usr/share/locale'

def init_logging(loglevel=logging.INFO):
    logfmt = '%(asctime)s %(levelname)s:%(module)s(%(lineno)s): %(message)s'
    datefmt = '%d/%m %H:%M:%S'
    # Remove existing handlers since it cause issues at least on debian dists
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    if DEVMODE:
        logging.basicConfig(format=logfmt, datefmt='%H:%M:%S',
                            level=logging.DEBUG)
    else:
        if not os.path.exists(CACHE_DIR):
            os.makedirs(CACHE_DIR)
        filehandler = RotatingFileHandler(
            os.path.join(CACHE_DIR, f'{PACKAGE_NAME}.log'), mode='a',
            maxBytes=1000000, backupCount=7, encoding='utf-8', delay=0)
        logging.basicConfig(format=logfmt, datefmt=datefmt, level=loglevel,
                            handlers=[filehandler])

def set_log_level(config):
    if DEVMODE:
        return
    logging.getLogger().setLevel(config.log_level)

def get_data_paths():
    if DEVMODE and hasattr(builtins, 'BB_DEVPATH'):
        return [os.path.join(builtins.BB_DEVPATH, 'data')] # pylint: disable=E1101
    return base.load_data_paths(PACKAGE_NAME)

def get_plugins_path():
    p_map = [os.path.join(CONFIG_DIR, 'plugins')]
    if DEVMODE and hasattr(builtins, 'BB_DEVPATH'):
        p_map.append(os.path.join(builtins.BB_DEVPATH, # pylint: disable=E1101
                                  f'{PACKAGE_NAME}/plugins'))
    elif not DEVMODE:
        p_map.append(os.path.join(get_python_lib(), f'{PACKAGE_NAME}/plugins'))
    return list(filter(os.path.exists, p_map))

def get_data_file(filename):
    for data_path in get_data_paths():
        file_path = os.path.join(data_path, filename)
        if os.path.exists(file_path):
            return file_path
    return None

def service_is_running():
    return dbus.SessionBus().name_has_owner(DBUS_BUS_NAME)

def shutdown_service():
    if not service_is_running():
        return
    logging.info('Shutting down existing service')
    bus = dbus.SessionBus()
    try:
        proxy = bus.get_object(DBUS_BUS_NAME, DBUS_OBJ_PATH)
        proxy.get_dbus_method('Shutdown', DBUS_BUS_NAME)()
        while bus.name_has_owner(DBUS_BUS_NAME):
            logging.info('Waiting existing service to shutdown...')
            print('Waiting existing service to shutdown...')
            time.sleep(2)
    except dbus.exceptions.DBusException:
        pass
    except Exception:
        logging.error('Exception while shutting down service', exc_info=True)

def version_checker(release_url, actual_version):
    ''' Return False if up to date, True otherwise '''
    try:
        req = requests.get(release_url, timeout=5)
    except (ConnErr, ConnectTimeout):
        return False
    response = req.json()
    if not response or not isinstance(response, list):
        return False
    last_version = response[0]['name'].lower().replace('v', '')
    if not last_version.replace('.', '').isdigit():
        return False
    return float(last_version) > float(actual_version)

def get_avatars():
    avatar_cmd = os.path.join(BIN_DIR, f'{PACKAGE_NAME}-avatar-provider')
    if not os.path.isfile(avatar_cmd):
        return dict()
    libs, error = Popen(['ldconfig', '-p'], stdout=PIPE,
                        stderr=PIPE).communicate()
    if not error and b'libfolks' not in libs:
        logging.warning('Libfolks not installed, avatars are disabled')
        return dict()
    cache_file = os.path.join(CACHE_DIR, f'avatars.cache')
    cache_content = pickle_read(cache_file)
    if cache_content:
        if  datetime.now() - cache_content['updated'] < timedelta(hours=1):
            logging.debug('Getting avatars from cache')
            return dict(filter(lambda i: os.path.isfile(i[1]),
                               cache_content['avatar_map'].items()))
    logging.info('Getting avatars')
    try:
        avatar_provider = Popen(avatar_cmd, stdout=PIPE, stderr=PIPE)
        output, error = avatar_provider.communicate(timeout=30)
    except TimeoutExpired:
        logging.error('Timeout getting avatars')
        return dict()
    except (OSError, ValueError):
        logging.error('Getting avatars:', exc_info=True)
        return dict()
    if error:
        logging.error(error.decode('utf-8').strip())
    if output:
        output = output.decode('utf-8').split(';')
    if not output or len(output) < 2:
        return dict()
    avatar_map = dict()
    output = iter(output)
    for folk in output:
        avatar_map[folk.lower()] = next(output)
    pickle_write({'updated': datetime.now(), 'avatar_map': avatar_map},
                 cache_file)
    return avatar_map

def pickle_read(pickle_file):
    try:
        with open(pickle_file, 'rb') as p_file:
            return pickle.load(p_file)
    except FileNotFoundError:
        return None
    except (PermissionError, pickle.UnpicklingError):
        logging.error(f'Reading {pickle_file}', exc_info=True)
        return None

def pickle_write(content, pickle_file):
    try:
        with open(pickle_file, 'wb') as p_file:
            pickle.dump(content, p_file)
    except (OSError, PermissionError, pickle.PicklingError):
        logging.error(f'Writing to {pickle_file}', exc_info=True)
