# Copyright 2012 Patrick Ulbrich <zulu99@gmx.net>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
import logging
import hashlib
from uuid import uuid4 as generate_uuid

import gi

gi.require_version('Secret', '1')
# pylint: disable=C0413
from gi.repository import Secret
try:
    gi.require_version('Goa', '1.0')
    from gi.repository import Goa
except (ValueError, ModuleNotFoundError):
    logging.warning('No gnome online accounts support found')
    Goa = None  # pylint: disable=C0103

from .config import PACKAGE_NAME, str_to_list
from .i18n import _


class SecretStore:
    def __init__(self):
        self.schema = Secret.Schema.new(
            f'org.framagit.{PACKAGE_NAME}', Secret.SchemaFlags.NONE,
            {"uuid": Secret.SchemaAttributeType.STRING})

    def save(self, account_name, uuid, password):
        if not uuid or not password:
            logging.error('Missing uuid or password')
            return
        Secret.password_store_sync(
            self.schema, {'uuid': uuid}, Secret.COLLECTION_DEFAULT,
            f'{PACKAGE_NAME.capitalize()} secret source for {account_name}',
            password, None)

    def password(self, uuid):
        return Secret.password_lookup_sync(self.schema, {'uuid': uuid}, None)

    def clear(self, uuid):
        return Secret.password_clear_sync(self.schema, {'uuid': uuid}, None)


class Account(dict):
    SECTION_NAME = 'Account '
    UUID = 'uuid'
    NAME = 'name'
    TYPE = 'type'
    ENABLED = 'enabled'
    BACKEND = 'backend'
    USER = 'user'
    PASS = 'password'
    SERVER = 'server'
    PORT = 'port'
    FOLDERS = 'folders'
    OAUTH2 = 'oauth2'
    INTERNAL = 'internal'
    GOA = 'goa'
    IMAP = 'imap'
    POP3 = 'pop3'
    BACKENDS = (IMAP, POP3)
    ERROR = 'error'

    KEYS = (NAME, TYPE, BACKEND, SERVER, USER, PASS, ERROR)
    OPT_KEYS = (ENABLED, PORT, FOLDERS)
    UNSAVED_KEYS = (PASS, FOLDERS)

    def __init__(self, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config = config
        for accountkey in self.KEYS + self.OPT_KEYS:
            if accountkey not in self or self[accountkey] == 'None':
                self[accountkey] = ''
        if self.UUID not in self or not self[self.UUID]:
            self[self.UUID] = str(generate_uuid())[:8]
        if not self[self.TYPE]:
            self[self.TYPE] = self.INTERNAL
        if not self[self.BACKEND]:
            self[self.BACKEND] = self.IMAP
        if not self[self.ENABLED]:
            self[self.ENABLED] = '1'
        self.secret_store = SecretStore()
        if self[self.TYPE] == self.INTERNAL and not self[self.PASS]:
            self[self.PASS] = self.secret_store.password(self[self.UUID])
        if not isinstance(self[self.FOLDERS], (dict, tuple, list)):
            if self[self.FOLDERS]:
                self[self.FOLDERS] = str_to_list(self[self.FOLDERS])
            else:
                self[self.FOLDERS] = []
        else:
            self[self.FOLDERS] = list(filter(len, self[self.FOLDERS]))
        self.section = self.SECTION_NAME + self[self.UUID]

    @classmethod
    def get(cls, config, uuid):
        section = cls.SECTION_NAME + uuid
        if not section in config:
            logging.debug('Account %s not found in config', uuid)
            return None
        return Account(config, dict(config.items(section)))

    @staticmethod
    def list(config):
        A = Account
        sections = filter(lambda s: A.SECTION_NAME in s, config.sections())
        accounts = map(lambda s: dict(config.items(s)), sections)
        for account in sorted(accounts, key=lambda a: a[A.NAME].lower()):
            account_class = A
            if account[A.TYPE] == A.GOA:
                if not Goa:
                    continue
                account_class = GnomeOnlineAccount
            yield account_class(config, dict(account))

    @property
    def is_locked(self):
        if self[self.TYPE] == self.GOA:
            return not (self[self.PASS] or self[self.OAUTH2])
        return not self[self.PASS]

    @property
    def is_valid(self):
        if list(filter(lambda k: k not in self, self.KEYS + self.OPT_KEYS)):
            return False
        filled_keys = filter(lambda k: k != self.ERROR, self.KEYS)
        if list(filter(lambda k: not self[k], filled_keys)):
            return False
        if self[self.BACKEND] not in self.BACKENDS:
            return False
        return True

    def delete(self, safe_call=True, commit=True):
        uuid = self[self.UUID]
        section = self.SECTION_NAME + uuid
        if safe_call and not self.config.has_section(section):
            logging.error(f'Unknown Account with id {uuid}')
            return
        self.config.remove_section(section)
        if self[self.TYPE] == self.INTERNAL:
            self.secret_store.clear(uuid)
        if commit:
            self.config.save()
        if self.config.dbus_service:
            self.config.dbus_service.Refresh()

    def save(self, commit=True):
        if self.config.has_section(self.section):
            self.delete(safe_call=False, commit=False)
        self.config.add_section(self.section)
        for saved_key in filter(lambda key: key not in self.UNSAVED_KEYS,
                                self.keys()):
            self.config.set(self.section, saved_key, self[saved_key])
        if self[self.BACKEND] == self.IMAP:
            if self[self.FOLDERS]:
                self.config.set(self.section, self.FOLDERS, self[self.FOLDERS])
            else:
                self.config.set(self.section, self.FOLDERS, '')
        if self[self.TYPE] == self.INTERNAL:
            self.secret_store.save(self[self.NAME], self[self.UUID],
                                   self[self.PASS])
        if commit:
            self.config.save()


class GnomeOnlineAccount(Account):
    KEYS = (Account.NAME, Account.TYPE, Account.ERROR)
    UNSAVED_KEYS = (Account.BACKEND, Account.USER, Account.SERVER, Account.PASS,
                    Account.OAUTH2, Account.PORT)
    OPT_KEYS = UNSAVED_KEYS + (Account.ENABLED, Account.FOLDERS)

    def __init__(self, config, *args, **kwargs):
        Account.__init__(self, config, *args, **kwargs)
        goa_account = list(filter(
            lambda a: self.get_uuid(a.get_mail().props) == self[self.UUID],
            self.goa_accounts()))
        if goa_account:
            self.goa_account = goa_account[0]
            goa_mail = self.goa_account.get_mail().props
            self[self.UUID] = self.get_uuid(goa_mail)
            self[self.USER] = goa_mail.imap_user_name
            self[self.SERVER] = goa_mail.imap_host
            self.get_credentials()

    def get_credentials(self):
        pwd_based = self.goa_account.get_password_based()
        if pwd_based:
            self[self.PASS] = pwd_based.call_get_password_sync('imap-password',
                                                               None)
            return
        try:
            oauth2_based = self.goa_account.get_oauth2_based()
            auth_token = oauth2_based.call_get_access_token_sync(None)
            user_name = self.goa_account.get_mail().props.imap_user_name
        except Exception:
            logging.error('Getting GOA credentials', exc_info=True)
            self[self.ERROR] = _('Credential error')
            self.save()
        else:
            self[self.OAUTH2] = (
                f'user={user_name}\1auth=Bearer {auth_token[0]}\1\1')
            if self[self.ERROR]:
                self[self.ERROR] = ''
                self.save()

    @classmethod
    def sync(cls, config, commit=True):
        # Clean up accounts deleted in GOA
        for account in cls.list(config):
            found = False
            for goa_account in cls.goa_accounts():
                goa_mail = goa_account.get_mail().props
                if account[cls.UUID] == cls.get_uuid(goa_mail):
                    found = True
                    break
            if not found:
                account.delete(commit=False)
        # Add new accounts for new GOA accounts
        for goa_account in cls.goa_accounts():
            goa_mail = goa_account.get_mail().props
            uuid = cls.get_uuid(goa_mail)
            if cls.get(config, uuid):
                logging.debug('Goa account for %s already registered',
                              goa_mail.email_address)
                continue
            logging.info('Creating new Gnome account for %s(%s)',
                         goa_mail.imap_user_name, goa_mail.imap_host)
            account_dict = {
                cls.BACKEND:cls.IMAP, cls.UUID:uuid, cls.TYPE:cls.GOA,
                cls.NAME: _('Gnome account {0}').format(goa_mail.email_address),
                cls.SERVER:goa_mail.imap_host, cls.USER:goa_mail.imap_user_name}
            account = cls(config, account_dict)
            account.section = cls.SECTION_NAME + uuid
            account.save(commit=False)
        if commit:
            config.save()

    @staticmethod
    def is_available():
        return bool(Goa)

    @classmethod
    def cleanup(cls, config):
        logging.debug('Cleanup GOA accounts')
        # Remove imported Goa accounts
        for account in cls.list(config):
            logging.info('Deleting %s', account[cls.NAME])
            account.delete(commit=False)

    @classmethod
    def list(cls, config):
        return filter(lambda a: a[a.TYPE] == a.GOA, super().list(config))

    @staticmethod
    def get_uuid(mail_account):
        digest = hashlib.md5(mail_account.email_address.encode('utf-8'))
        return digest.hexdigest()[:8]

    @staticmethod
    def goa_accounts():
        client = Goa.Client.new_sync(None)
        accounts = client.get_accounts()
        for account in accounts:
            mail = account.get_mail() # get the mail interface
            if (not mail or account.get_account().props.mail_disabled
                    or not mail.props.imap_supported):
                continue
            yield account
