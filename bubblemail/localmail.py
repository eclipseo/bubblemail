# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
import logging
import getpass
import mailbox

from .i18n import _


class LocalMail:
    """Implementation of local mailboxes, like mbox and maildir."""
    LOCAL = _('Local mail')
    def __init__(self, config):
        """Initialize mbox mailbox backend with a name and path."""
        self.mbox_files = (os.path.join(os.getenv('HOME'), '.mbox'),
                           os.path.join('/var/spool/mail', getpass.getuser()))
        maildir_path = os.path.join(os.getenv('HOME'), 'Maildir')
        self.mdir_path = maildir_path if os.path.isdir(maildir_path) else None
        self.mbox_files = list(filter(os.path.isfile, self.mbox_files))
        self.config = config

    @property
    def available(self):
        return bool(self.mdir_path or self.mbox_files)

    def list_messages(self):
        """List unread messages from the mailbox.
        Yields pairs (folder, message) where folder is always ''.
        """
        if not self.available:
            logging.debug('No local mail folder/file found')
            return
        for mbox_file in self.mbox_files:
            logging.debug('Checking mbox %s', mbox_file)
            mbox = mailbox.mbox(mbox_file, create=False)
            for mail in mbox:
                if 'R' not in mail.get_flags():
                    yield '', mail
            mbox.close()
        if not self.mdir_path:
            return
        folders = self.config.get_core(self.config.LOCAL_FOLDERS)
        folders = folders or ['']
        maildir = mailbox.Maildir(self.mdir_path, factory=None, create=False)
        for folder in folders:
            try:
                for mail in self._get_folder(maildir, folder):
                    if 'S' not in mail.get_flags():
                        yield folder, mail
            except (FileNotFoundError, PermissionError):
                logging.exception(
                    f'Getting local folder {self.mdir_path}/{folder}',
                    exc_info=True)
                continue
        maildir.close()

    def request_folders(self):
        """Lists folders from maildir."""
        if not self.mdir_path:
            return None
        maildir = mailbox.Maildir(self.mdir_path, factory=None, create=False)
        folders = [''] + maildir.list_folders()
        maildir.close()
        return folders

    def _get_folder(self, maildir, folder):
        if not folder or folder.lower() == 'inbox':
            return maildir
        try:
            return maildir.get_folder(folder)
        except mailbox.NoSuchMailboxError:
            logging.error(f'Local folder {folder} not found, removing it')
            selected_folders = self.config.get_core(self.config.LOCAL_FOLDERS)
            if folder in selected_folders:
                selected_folders.remove(folder)
            self.config.set_core(self.config.LOCAL_FOLDERS, selected_folders)
            self.config.save(force=True)
            return list()
